# Travel Nest Challenge

## Specification

Create a web scraper that will extract the property name, property type (e.g. Apartment),
number of bedrooms, bathrooms and list of amenities from the following properties.

- https://www.airbnb.co.uk/rooms/14531512?s=51
- https://www.airbnb.co.uk/rooms/19278160?s=51
- https://www.airbnb.co.uk/rooms/19292873?s=51

## Installation

We require python 3 to run this application and pip to install our dependencies.

### Virtualenv

You may skip this step however, it's good practice to isolate project dependencies from
other projects.

In the root of your project, create your virtualenv:

```
virtualenv -p python3 venv
```

The environment can now be activated.

```
source venv/bin/activate
```

### Installing the Dependencies

We will require the Firefox driver (geckodriver) interface to run the code. Installation
instructions for that can be found [here](http://selenium-python.readthedocs.io/installation.html#drivers).

Install our dependencies into our environment.

```
pip install -r requirements.txt
```

## How to Run

A small code snippet has been written to print out the details of the properties from the
specification. This can be run with the command.

```
python travel_nest_challenge.py
```

Alternatively you can run the scraper tests to check the results against our test data.

```
python -m unittest tests.ScrapePropertyTest
```

## Notes

On an initial investigation of the DOM of each property, I noticed that the property
name, property type, number of bedrooms and number of bathrooms are all contained within
a <div> where the id='summary'. I will first target that <div> to find the others
underneath.

Getting the full list of amenities is a little bit trickier as you need to first click a
'show more' button to generate the data on to the page. Once generated, all the active
amenities will be wrapped in a <strong> under a <div> with a class attribute of
'amenities'.

Based on the above notes, I will rule out the 'requests' modules + html parser such as
BeautifulSoup or Scrapy as it's harder to load dynamic data. Instead I will use 'selenium'
since that is able to do this for me.

Assumptions made for final solution:

- The DOM of the Airbnb 'rooms' page will not change.
- Airbnb are not running multiple DOMs in sequence to analysis what UI works best.
