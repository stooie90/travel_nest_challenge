# -*- coding: utf-8 -*-
"""
A module containing all the code required for the challenge.
"""
import re

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait


class Property:
    """Represents a property with the required attributes"""

    def __init__(self, name, type, n_guests, n_beds, n_bathrooms, amenities):
        self.name = name
        self.type = type
        self.n_guests = n_guests
        self.n_beds = n_beds
        self.n_bathrooms = n_bathrooms
        self.amenities = amenities

    def __str__(self):
        return "<Property: name='{}'>".format(self.name)


def init_driver():
    """
    Creates an instance of a Firefox selenium webdriver with a timeout of 5 seconds.
    """
    # we are using the Firefox driver since it is the default browser on linux
    driver = webdriver.Firefox()
    driver.wait = WebDriverWait(driver, 5)
    return driver


def scrape_property(driver, url):
    """
    Scrapes the details of a property from the provided url. The expected url should
    link to an Airbnb property.

    :param driver: Selenium driver
    :param url: URL to an Airbnb property
    :return: Object containing the property details.
    :rtype: Property
    """
    # send a request to load the web page.
    driver.get(url)

    # the div where id='summary' contains the property name, property type, number of
    # bedrooms and bathrooms
    property_name = driver.find_element_by_xpath(
        '//div[@id="summary"]//div[@itemprop="name"]/span'
    ).text

    # get the text value for each of the property details
    property_details = [
        d.text for d in
        driver.find_elements_by_xpath('//div[@id="summary"]//span[@class="_y8ard79"]')
    ]

    # we know the details are in order of:
    # 1. number of guests
    # 2. property type
    # 3. number of bedrooms
    # 4. number of bathrooms

    # where appropriate select only the number
    re_ints = re.compile(r'\d+')

    property_type = property_details[1]
    n_guests = int(re_ints.match(property_details[0]).group(0))
    n_beds = int(re_ints.match(property_details[2]).group(0))
    n_bathrooms = int(re_ints.match(property_details[3]).group(0))

    # to get a full list of amenities, we need to click the show more button
    elm_more_amenities = driver.find_element_by_xpath('//div[@class="amenities"]//button')
    elm_more_amenities.click()

    # all available amenities will be wrapped around the '<strong>' tag
    elm_amenities = driver.find_elements_by_xpath('//div[@class="amenities"]//strong')
    amenities = [a.text for a in elm_amenities]

    return Property(name=property_name, type=property_type, n_guests=n_guests,
                    n_beds=n_beds, n_bathrooms=n_bathrooms, amenities=amenities)


if __name__ == '__main__':
    driver = init_driver()

    urls = [
        'https://www.airbnb.co.uk/rooms/14531512?s=51',
        'https://www.airbnb.co.uk/rooms/19278160?s=51',
        'https://www.airbnb.co.uk/rooms/19292873?s=51'
    ]

    properties = (scrape_property(driver, url) for url in urls)

    for p in properties:
        print('Name: %s' % p.name)
        print('Type: %s' % p.type)
        print('No. Bathrooms: %s' % p.n_bathrooms)
        print('No. Beds: %s' % p.n_beds)
        print('No. Guests: %s' % p.n_guests)
        print('List of Amenities:\n\t' + '\n\t'.join(p.amenities))
        print()

    # close the driver
    driver.quit()
