# -*- coding: utf-8 -*-
"""
Collection of test cases for the coding challenge.
"""
import unittest

from travel_nest_challenge import Property, init_driver, scrape_property


class PropertyTest(unittest.TestCase):
    """Test cases for the Property model."""

    def test_property_creation(self):
        """Test case for the initialisation of a Property class"""

        property_name = "TravelNest"
        property_type = "Office"
        n_guests = 4
        n_beds = 0
        n_bathrooms = 1
        amenities = ['receptionist', 'whiteboard', 'meeting room']

        p = Property(name=property_name, type=property_type, n_guests=n_guests,
                     n_beds=n_beds, n_bathrooms=n_bathrooms, amenities=amenities)

        self.assertEqual(p.name, property_name)
        self.assertEqual(p.type, property_type)
        self.assertEqual(p.n_guests, n_guests)
        self.assertEqual(p.n_beds, n_beds)
        self.assertEqual(p.n_bathrooms, n_bathrooms)
        self.assertListEqual(p.amenities, amenities)


class InitDriverTest(unittest.TestCase):
    """Tests the setup of the selenium driver"""

    def test_init_driver(self):
        driver = init_driver()
        driver.get('https://travelnest.com/')

        self.assertTrue('TravelNest' in driver.title)

        driver.close()


class ScrapePropertyTest(unittest.TestCase):
    """Tests for scraping the property info"""

    def setUp(self):
        self.driver = init_driver()
        # list of test urls
        self.urls = [
            'https://www.airbnb.co.uk/rooms/14531512?s=51',
            'https://www.airbnb.co.uk/rooms/19278160?s=51',
            'https://www.airbnb.co.uk/rooms/19292873?s=51'
        ]
        # list of expected property details from our test urls.
        self.properties = [
            Property(name='Garden Rooms: Featured in Grand Designs Sept 2017',
                     type='Studio', n_guests=2, n_beds=1, n_bathrooms=1,
                     amenities=[
                         'Family/Kid Friendly',
                         'Wheelchair accessible',
                         'Suitable for events',
                         'Kitchen',
                         'Wireless Internet',
                         'Cable TV',
                         'Hair dryer',
                         'TV',
                         'Laptop friendly workspace',
                         'Iron',
                         'Essentials',
                         'Shampoo',
                         'Hangers',
                         'Heating',
                         'Private entrance',
                         'High chair',
                         'Pack ’n Play/travel crib'
                     ]),
            Property(name='York Place: Presidential Suite For Two',
                     type='1 bedroom', n_guests=2, n_beds=1, n_bathrooms=1,
                     amenities=[
                         'Kitchen',
                         'Wireless Internet',
                         'Hair dryer',
                         'TV',
                         'Laptop friendly workspace',
                         'Iron',
                         'Essentials',
                         'Shampoo',
                         'Washer',
                         'Hangers',
                         'Heating',
                         'Private entrance'
                     ]),
            Property(name='Turreted apartment near Edinburgh Castle',
                     type='1 bedroom', n_guests=3, n_beds=1, n_bathrooms=1,
                     amenities=[
                         'Family/Kid Friendly',
                         'Kitchen',
                         'Wireless Internet',
                         'Breakfast',
                         'Hair dryer',
                         'Laptop friendly workspace',
                         'Essentials',
                         'Shampoo',
                         'Washer',
                         'Hangers',
                         'Heating'
                     ]),
        ]

    def tearDown(self):
        self.driver.close()

    def test_scrape_property(self):
        """Tests scraping the first property in the specification"""
        # loop each url and compare the property with our expected results.
        for i, url in enumerate(self.urls):
            p = scrape_property(self.driver, url)
            test_p = self.properties[i]

            self.assertEqual(p.name, test_p.name)
            self.assertEqual(p.type, test_p.type)
            self.assertEqual(p.n_guests, test_p.n_guests)
            self.assertEqual(p.n_beds, test_p.n_beds)
            self.assertEqual(p.n_bathrooms, test_p.n_bathrooms)
            # sort both of the lists in case the order isn't exact
            self.assertListEqual(sorted(p.amenities), sorted(test_p.amenities))


if __name__ == '__main__':
    unittest.main()
